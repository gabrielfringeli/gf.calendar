<?php
namespace GF\Calendar\Eel\Helper;

use Neos\Flow\Annotations as Flow;
use Neos\Eel\ProtectedContextAwareInterface;
use GF\Calendar\Domain\Repository\CalendarRepository;
use GF\Calendar\Domain\Repository\EventRepository;
use Neos\Flow\Persistence\QueryInterface;

class CalendarHelper implements ProtectedContextAwareInterface {

    /**
     * @Flow\Inject
     * @var CalendarRepository
     */
    protected $calendarRepository;

    /**
     * @Flow\Inject
     * @var EventRepository
     */
    protected $eventRepository;

    /**
    * Get all events
    *
    * @return array
    */
    public function events() {

        $events = $this->eventRepository->findAll();

        $jsonEvents = array();
        foreach ($events as $event) {
            array_push($jsonEvents, $event->toObject());
        }
        
        return $jsonEvents;
    }

    /**
    * Get events running at the moment.
    *
    * @return array
    */
    public function currentEvents() {
        $events = $this->eventRepository->findAll();

        $jsonEvents = array();
        $now = new \DateTime();
        foreach ($events as $event) {
            if ($event->relativeToDate($now) == 0) {
                array_push($jsonEvents, $event->toObject());
            }
        }
        
        return $jsonEvents;
    }

    /**
    * Get future events.
    *
    * @return array
    */
    public function futureEvents() {
        $events = $this->eventRepository->findAllSorted(array('begin' => QueryInterface::ORDER_ASCENDING));

        $jsonEvents = array();
        $now = new \DateTime();
        foreach ($events as $event) {
            if ($event->relativeToDate($now) == 1) {
                array_push($jsonEvents, $event->toObject());
            }
        }
        
        return $jsonEvents;
    }
    
    /**
    * All methods are considered safe, i.e. can be executed from within Eel
    *
    * @param string $methodName
    * @return boolean
    */
    public function allowsCallOfMethod($methodName) {
            return TRUE;
    }

}