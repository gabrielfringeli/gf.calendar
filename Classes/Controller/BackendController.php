<?php
namespace GF\Calendar\Controller;

/*
 * This file is part of the GF.Calendar package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use GF\Calendar\Domain\Model\Calendar;
use GF\Calendar\Domain\Model\Event;
use GF\Calendar\Domain\Repository\CalendarRepository;
use GF\Calendar\Domain\Repository\EventRepository;

class BackendController extends ActionController
{

    /**
     * @Flow\Inject
     * @var CalendarRepository
     */
    protected $calendarRepository;

    /**
     * @Flow\Inject
     * @var EventRepository
     */
    protected $eventRepository;

    /*
     * Calendar management
     * ----------------
     */

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('calendars', $this->calendarRepository->findAll());
        $this->view->assign('events', $this->eventRepository->findAll());
    }

    /**
     * @param \GF\Calendar\Domain\Model\Calendar $calendar
     * @return void
     */
    public function showAction(Calendar $calendar)
    {
        $this->view->assign('calendar', $calendar);
    }

    /**
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * @param Calendar $newCalendar
     * @return void
     */
    public function createAction(Calendar $newCalendar)
    {
        $this->calendarRepository->add($newCalendar);
        $this->addFlashMessage('Created a new calendar.');
        $this->redirect('index');
    }

    /**
     * @param Calendar $calendar
     * @return void
     */
    public function editAction(Calendar $calendar)
    {
        $this->view->assign('calendar', $calendar);
    }

    /**
     * @param Calendar $calendar
     * @return void
     */
    public function updateAction(Calendar $calendar)
    {
        $this->calendarRepository->update($calendar);
        $this->addFlashMessage('Updated the calendar.');
        $this->redirect('index');
    }

    /**
     * @param Calendar $calendar
     * @return void
     */
    public function deleteAction(Calendar $calendar)
    {
        $this->calendarRepository->remove($calendar);
        $this->addFlashMessage('Deleted a calendar.');
        $this->redirect('index');
    }


    /*
     * Event management
     * ----------------
     */

    /**
     * @param Event $event
     * @return void
     */
    public function showEventAction(Event $event)
    {
        $this->view->assign('event', $event);
    }

    /**
     * @return void
     */
    public function newEventAction()
    {
        $this->view->assign('calendars', $this->calendarRepository->findAll());
    }

    /**
     * @param Event $newEvent
     * @return void
     */
    public function createEventAction(Event $newEvent)
    {
        $this->eventRepository->add($newEvent);
        $this->addFlashMessage('Created a new event.');
        $this->redirect('index');
    }

    /**
     * @param Event $event
     * @return void
     */
    public function editEventAction(Event $event)
    {
        $this->view->assign('calendars', $this->calendarRepository->findAll());
        $this->view->assign('event', $event);
    }

    /**
     * @param Event $event
     * @return void
     */
    public function updateEventAction(Event $event)
    {
        $this->eventRepository->update($event);
        $this->addFlashMessage('Updated the event.');
        $this->redirect('index');
    }

    /**
     * @param Event $event
     * @return void
     */
    public function deleteEventAction(Event $event) {
        $this->eventRepository->remove($event);
        $this->addFlashMessage('Deleted an event.');
        $this->redirect('index');
    }

}
