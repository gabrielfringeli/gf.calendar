<?php
namespace GF\Calendar\Controller;

/*
 * This file is part of the GF.Calendar package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use GF\Calendar\Domain\Model\Calendar;
use GF\Calendar\Domain\Model\Event;
use GF\Calendar\Domain\Repository\CalendarRepository;
use GF\Calendar\Domain\Repository\EventRepository;

class PluginController extends ActionController
{

    /**
     * @Flow\Inject
     * @var CalendarRepository
     */
    protected $calendarRepository;

    /**
     * @Flow\Inject
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('events', $this->eventRepository->findFutureAndActive());
    }

    /**
     * @param Event $event
     * @return void
     */
    public function showEventAction(Event $event)
    {
        $this->view->assign('event', $event);
    }

}
