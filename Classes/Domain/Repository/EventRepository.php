<?php
namespace GF\Calendar\Domain\Repository;

/*
 * This file is part of the GF.Calendar package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\QueryResultInterface;

/**
 * @Flow\Scope("singleton")
 */
class EventRepository extends Repository
{

	public function __construct()
    {
    	parent::__construct();
        $this->setDefaultOrderings(array(
			'begin' => QueryInterface::ORDER_DESCENDING
		));
    }

    public function findFutureAndActive() {
    	$query = $this->createQuery();

    	$now = new \DateTime();

    	return $query->matching($query->greaterThan('end', $now))->setOrderings(array('begin' => QueryInterface::ORDER_ASCENDING))->execute();
	}

	/*
	* @param array $sorting
	*/
	
	public function findAllSorted($sorting) {
		$query = $this->createQuery();

		return $query->setOrderings($sorting)->execute();
	}

}
