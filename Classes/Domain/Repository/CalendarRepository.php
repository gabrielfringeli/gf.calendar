<?php
namespace GF\Calendar\Domain\Repository;

/*
 * This file is part of the GF.Calendar package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class CalendarRepository extends Repository
{

    public function __construct()
    {
    	parent::__construct();
        $this->setDefaultOrderings(array(
        		'name' => \Neos\Flow\Persistence\QueryInterface::ORDER_ASCENDING
        	));
    }

}
