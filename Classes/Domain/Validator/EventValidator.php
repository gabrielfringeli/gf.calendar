<?php

namespace GF\Calendar\Domain\Validator;

use Neos\Flow\Validation\Validator\AbstractValidator;
use GF\Calendar\Domain\Model\Event;

class EventValidator extends AbstractValidator
{
    protected function isValid($event)
    {
        if (! $event instanceof Event) {
            $this->addError('An event must be of type GF\Calendar\Domain\Model\Event', time());
        } else {
            if ($event->getEnd() < $event->getBegin()) {
                $this->addError("An event's start can't be later than its end.", time());
            }
        }
    }
}