<?php
namespace GF\Calendar\Domain\Model;

/*
 * This file is part of the GF.Calendar package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use GF\Calendar\Domain\Model\Calendar;
use Neos\Flow\I18n\Formatter\DatetimeFormatter;
use Neos\Flow\I18n\Locale;
use Neos\Flow\Persistence\PersistenceManagerInterface;

/**
 * @Flow\Entity
 */
class Event
{
    /**
     * @Flow\Inject
     * @var PersistenceManagerInterface
     */
	protected $persistenceManager;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="Text")
     * @Flow\Validate(type="StringLength", options={"minimum"=1, "maximum"=100})
     * @ORM\Column(length=100)
     * @var string
     */
    protected $title;

    /**
     * @var boolean
     */
    protected $allDay = false;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="DateTime")
     * @var \DateTime
     */
    protected $begin;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="DateTime")
     * @var \DateTime
     */
    protected $end;

    /**
     * @Flow\Validate(type="Text")
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    protected $description;

    /**
     * @Flow\Validate(type="Text")
     * @Flow\Validate(type="StringLength", options={"maximum"=100})
     * @ORM\Column(nullable=true)
     * @var string
     */
    protected $location;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @ORM\ManyToOne(inversedBy="events")
     * @var Calendar
     */
    protected $calendar;

    /**
     * @Flow\Inject
     * @var \Neos\Flow\I18n\Service
     */
    protected $i18nService;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->persistenceManager->getIdentifierByObject($this);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return boolean
     */
    public function getAllDay(): bool
    {
        return $this->allDay;
    }

    /**
     * @param boolean $allDay
     * @return void
     */
    public function setAllDay(bool $allDay)
    {
        $this->allDay = $allDay;
    }

    /**
     * @return \DateTime
     */
    public function getBegin(): \DateTime
    {
        return $this->begin;
    }

    /**
     * @param \DateTime $begin
     * @return void
     */
    public function setBegin(\DateTime $begin)
    {
        $this->begin = $begin;
    }

    /**
     * @return \DateTime
     */
    public function getEnd(): \DateTime
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     * @return void
     */
    public function setEnd(\DateTime $end)
    {
        $this->end = $end;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return void
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param null|string $location
     * @return void
     */
    public function setLocation(string $location=null)
    {
        $this->location = $location;
    }

    /**
     * @return null|Calendar
     */
    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }

    /**
     * @param null|Calendar $calendar
     * @return void
     */
    public function setCalendar($calendar)
    {
        $this->calendar = $calendar;
    }

    /**
     * Returns a human readable string with time information about this event.
     *
     * @return string
     */
    public function getFormattedTimeInformation(): string
    {

        $locale = $this->i18nService->getConfiguration()->getCurrentLocale();

        $datetimeFormatter = new DatetimeFormatter();

        $beginDateString = $datetimeFormatter->formatDateTimeWithCustomPattern($this->begin, "EE d. MMMM y", $locale);
        $endDateString = $datetimeFormatter->formatDateTimeWithCustomPattern($this->end, "EE d. MMMM y", $locale);

        $timeInfo = $beginDateString;

        if ($this->allDay) {

            // all-day event -> time not important
            if (strcmp($beginDateString, $endDateString) !== 0) {
                // begin and end date are not the same -> include both
                $timeInfo .= (" - " . $endDateString);
            }

        } else {
            // time and date information needed
            $beginTimeString = $datetimeFormatter->formatDateTimeWithCustomPattern($this->begin, "H:mm", $locale);
            $endTimeString = $datetimeFormatter->formatDateTimeWithCustomPattern($this->end, "H:mm", $locale);

            $timeInfo .= ", " . $beginTimeString . " - ";

            if (strcmp($beginDateString, $endDateString) !== 0) {
                // begin and end date are not the same -> include both
                $timeInfo .= $endDateString . ", ";
            }

            $timeInfo .= $endTimeString;
        }

        return $timeInfo;
    }

    /**
    * Returns 0 if date is inbetween start and end of this event.
    * Returns 1 if this event begins after date
    * Returns -1 if this event ended before date
    * @param \DateTime $date
    * @return integer
    */
    public function relativeToDate($date) {

        $beginRef = clone $this->begin;
        $endRef = clone $this->end;

        if ($this->allDay) {
            // all-day event
            $beginRef->setTime(0,0,0);
            $endRef->modify('+1 day');
            $endRef->setTime(0,0,0);

        }

        // event with exact start and end
        if ($endRef < $date) {
            return -1;
        } else if ($beginRef > $date) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns a Object representation of this event
     * @return string
     */
    public function toObject() {
        return [
            'identifier' => $this->getIdentifier(),
            'title' => $this->title,
            'allDay' => $this->allDay,
            'begin' => $this->begin,
            'end' => $this->end,
            'formattedTimeInformation' => $this->getFormattedTimeInformation(),
            'description' => $this->description,
            'location' => $this->location,
            'calendar' => array(
                'name' => $this->calendar->getName(),
                'id' => $this->calendar->getIdentifier(),
                'color' => $this->calendar->getColor()->getHtmlColorString()
            )
        ];
    }
}
