<?php
namespace GF\Calendar\Domain\Model;

/*
 * This file is part of the GF.Calendar package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Neos\Flow\Persistence\PersistenceManagerInterface;

/**
 * @Flow\Entity
 */
class Calendar
{
    /**
     * @Flow\Inject
     * @var PersistenceManagerInterface
     */
	protected $persistenceManager;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="Text")
     * @Flow\Validate(type="StringLength", options={"minimum"=1, "maximum"=100})
     * @ORM\Column(length=100)
     * @var string
     */
    protected $name;

    /**
     * @ORM\OneToMany(mappedBy="calendar")
     * @ORM\OrderBy({"begin" = "DESC"})
     * @var Collection<Event>
     */
    protected $events;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @var Color
     */
    protected $color;

    /**
     * Calendar constructor.
     * @param $name
     * @param Color $color
     */
    public function __construct(string $name, Color $color) {
        $this->name = $name;
        $this->color = $color;
        $this->events = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->persistenceManager->getIdentifierByObject($this);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    /**
     * @param Collection $events
     * @return void
     */
    public function setEvents(Collection $events)
    {
        $this->events = $events;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @param Color $color
     */
    public function setColor(Color $color)
    {
        $this->color = $color;
    }
}
