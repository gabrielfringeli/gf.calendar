<?php
namespace GF\Calendar\Domain\Model;

/*
 * This file is part of the GF.Calendar package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\ValueObject
 */
class Color
{

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="NumberRange", minimum=0, maximum=255)
     * @Flow\Validate(type="Integer")
     * @var integer
     */
    protected $red;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="NumberRange", minimum=0, maximum=255)
     * @Flow\Validate(type="Integer")
     * @var integer
     */
    protected $green;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="NumberRange", minimum=0, maximum=255)
     * @Flow\Validate(type="Integer")
     * @var integer
     */
    protected $blue;

    /**
     * Color constructor.
     * @param int $red
     * @param int $green
     * @param int $blue
     */
    public function __construct(int $red, int $green, int $blue)
    {
        if ($red >= 0 && $red <= 255 && $green >= 0 && $green <= 255 && $blue >= 0 && $blue <=255) {
            $this->red = $red;
            $this->green = $green;
            $this->blue = $blue;
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * @return integer
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * @return integer
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * @return string
     */
    public function getHtmlColorString(): string {
        return sprintf("#%02x%02x%02x", $this->red, $this->green, $this->blue);
    }

}
