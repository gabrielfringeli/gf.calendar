<?php
namespace GF\Calendar\Domain\TypeConverter;

use Neos\Flow\Property\TypeConverter\AbstractTypeConverter;
use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages\Error;
use Neos\Flow\Property\PropertyMappingConfigurationInterface;
use GF\Calendar\Domain\Model\Color;

/**
 * Converter which transforms to a Color object.
 *
 * Input values may be:
 * - 6-digit hex string with optional leading hash ('#'), e.g. #4345AA
 * - integer in range of [0, xFFFFFF]
 *
 * @api
 * @Flow\Scope("singleton")
 */
class ColorConverter extends AbstractTypeConverter
{
    /**
     * @var array<string>
     */
    protected $sourceTypes = ['integer', 'string'];

    /**
     * @var string
     */
    protected $targetType = Color::class;

    /**
     * @var integer
     */
    protected $priority = 200;

    /**
     * Actually convert from $source to $targetType
     *
     * @param mixed $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param PropertyMappingConfigurationInterface $configuration
     * @return Color|Error
     * @api
     */
    public function convertFrom($source, $targetType, array $convertedChildProperties = [], PropertyMappingConfigurationInterface $configuration = null)
    {
        if (is_int($source)) {
            return $this->createColorWithInt($source);
        }

        if (is_string($source)) {

            if ($source[0] === '#') {
                $source = substr($source, 1);
            }

            if (strlen($source) === 6 && ctype_xdigit($source)) {
                return $this->createColorWithInt(hexdec($source));
            } else {
                return new Error('"%s" is not a valid RGB string', null, [$source]);
            }

        }

        if ($source === null || strlen($source) === 0) {
            return null;
        }
    }

    /**
     * Create Color Object from integer value
     *
     * @param int $value
     * @return Color|Error
     */
    private function createColorWithInt(int $value)
    {
        // $value must be in range (0, xFFFFFF = 16'777'215) to represent a RGB color value.
        if ($value >= 0 && $value <= 16777215) {

            $red = 0xFF & ($value >> 0x10);
            $green = 0xFF & ($value >> 0x8);
            $blue = 0xFF & $value;

            return new Color($red, $green, $blue);

        } else {
            return new Error('%d is not in supported color range [0, 16\'581\'375]', null, [$value]);
        }
    }
}
