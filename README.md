#GF.Calendar
This is a Neos CMS package for managing calendars.

##Features

* Neos CMS backend module for managing calendars and their events
* NodeTypes to render your Calendars and Events in your Website

##Installation
Add this package to your project composer requirements.

```bash
composer require gf/calendar
```

Go to the root folder of this package and install Bower dependencies.

```bash
cd Packages/Application/GF.Calendar
bower install
```